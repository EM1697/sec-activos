﻿namespace SEC_Activos
{
    partial class formCalendario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formCalendario));
            this.panelFecha = new System.Windows.Forms.Panel();
            this.cboxMesFecha = new System.Windows.Forms.ComboBox();
            this.txbYearFecha = new System.Windows.Forms.TextBox();
            this.txbDiaFecha = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.calendarFecha = new System.Windows.Forms.MonthCalendar();
            this.cbFecha = new System.Windows.Forms.CheckBox();
            this.panelRango = new System.Windows.Forms.Panel();
            this.cbFechaFinal = new System.Windows.Forms.CheckBox();
            this.cbFechaInicial = new System.Windows.Forms.CheckBox();
            this.panelRangoFin = new System.Windows.Forms.Panel();
            this.cboxMesRangoFin = new System.Windows.Forms.ComboBox();
            this.txbYearRangoFin = new System.Windows.Forms.TextBox();
            this.txbDiaRangoFin = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panelRangoInicio = new System.Windows.Forms.Panel();
            this.cboxMesRangoInicio = new System.Windows.Forms.ComboBox();
            this.txbYearRangoInicio = new System.Windows.Forms.TextBox();
            this.txbDiaRangoInicio = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.calendarRango = new System.Windows.Forms.MonthCalendar();
            this.cbRango = new System.Windows.Forms.CheckBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.panelFecha.SuspendLayout();
            this.panelRango.SuspendLayout();
            this.panelRangoFin.SuspendLayout();
            this.panelRangoInicio.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelFecha
            // 
            this.panelFecha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelFecha.Controls.Add(this.cboxMesFecha);
            this.panelFecha.Controls.Add(this.txbYearFecha);
            this.panelFecha.Controls.Add(this.txbDiaFecha);
            this.panelFecha.Controls.Add(this.label3);
            this.panelFecha.Controls.Add(this.label2);
            this.panelFecha.Controls.Add(this.label1);
            this.panelFecha.Controls.Add(this.calendarFecha);
            this.panelFecha.Controls.Add(this.cbFecha);
            this.panelFecha.Location = new System.Drawing.Point(12, 12);
            this.panelFecha.Name = "panelFecha";
            this.panelFecha.Size = new System.Drawing.Size(842, 238);
            this.panelFecha.TabIndex = 0;
            this.panelFecha.Tag = "";
            // 
            // cboxMesFecha
            // 
            this.cboxMesFecha.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboxMesFecha.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboxMesFecha.FormattingEnabled = true;
            this.cboxMesFecha.Items.AddRange(new object[] {
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"});
            this.cboxMesFecha.Location = new System.Drawing.Point(215, 99);
            this.cboxMesFecha.Name = "cboxMesFecha";
            this.cboxMesFecha.Size = new System.Drawing.Size(121, 24);
            this.cboxMesFecha.TabIndex = 8;
            this.cboxMesFecha.TextChanged += new System.EventHandler(this.cboxMesFecha_TextChanged);
            // 
            // txbYearFecha
            // 
            this.txbYearFecha.Location = new System.Drawing.Point(215, 149);
            this.txbYearFecha.MaxLength = 4;
            this.txbYearFecha.Name = "txbYearFecha";
            this.txbYearFecha.Size = new System.Drawing.Size(100, 22);
            this.txbYearFecha.TabIndex = 7;
            this.txbYearFecha.TextChanged += new System.EventHandler(this.txbYearFecha_TextChanged);
            // 
            // txbDiaFecha
            // 
            this.txbDiaFecha.Location = new System.Drawing.Point(215, 59);
            this.txbDiaFecha.MaxLength = 2;
            this.txbDiaFecha.Name = "txbDiaFecha";
            this.txbDiaFecha.Size = new System.Drawing.Size(100, 22);
            this.txbDiaFecha.TabIndex = 5;
            this.txbDiaFecha.TextChanged += new System.EventHandler(this.txbDiaFecha_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(115, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 24);
            this.label3.TabIndex = 4;
            this.label3.Text = "Año";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(115, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 24);
            this.label2.TabIndex = 3;
            this.label2.Text = "Mes";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(115, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "Día";
            // 
            // calendarFecha
            // 
            this.calendarFecha.Location = new System.Drawing.Point(524, 9);
            this.calendarFecha.Name = "calendarFecha";
            this.calendarFecha.TabIndex = 1;
            this.calendarFecha.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.calendarFecha_DateChanged);
            // 
            // cbFecha
            // 
            this.cbFecha.AutoSize = true;
            this.cbFecha.Checked = true;
            this.cbFecha.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbFecha.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbFecha.Enabled = false;
            this.cbFecha.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFecha.Location = new System.Drawing.Point(0, 0);
            this.cbFecha.Name = "cbFecha";
            this.cbFecha.Size = new System.Drawing.Size(840, 28);
            this.cbFecha.TabIndex = 0;
            this.cbFecha.Text = "Fecha";
            this.cbFecha.UseVisualStyleBackColor = true;
            this.cbFecha.CheckedChanged += new System.EventHandler(this.cbFecha_CheckedChanged);
            // 
            // panelRango
            // 
            this.panelRango.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelRango.Controls.Add(this.cbFechaFinal);
            this.panelRango.Controls.Add(this.cbFechaInicial);
            this.panelRango.Controls.Add(this.panelRangoFin);
            this.panelRango.Controls.Add(this.panelRangoInicio);
            this.panelRango.Controls.Add(this.calendarRango);
            this.panelRango.Controls.Add(this.cbRango);
            this.panelRango.Location = new System.Drawing.Point(13, 265);
            this.panelRango.Name = "panelRango";
            this.panelRango.Size = new System.Drawing.Size(840, 392);
            this.panelRango.TabIndex = 1;
            // 
            // cbFechaFinal
            // 
            this.cbFechaFinal.AutoSize = true;
            this.cbFechaFinal.Enabled = false;
            this.cbFechaFinal.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFechaFinal.Location = new System.Drawing.Point(523, 308);
            this.cbFechaFinal.Name = "cbFechaFinal";
            this.cbFechaFinal.Size = new System.Drawing.Size(116, 25);
            this.cbFechaFinal.TabIndex = 5;
            this.cbFechaFinal.Text = "Fecha Final";
            this.cbFechaFinal.UseVisualStyleBackColor = true;
            this.cbFechaFinal.Visible = false;
            this.cbFechaFinal.Click += new System.EventHandler(this.CbFechaFinal_Click);
            // 
            // cbFechaInicial
            // 
            this.cbFechaInicial.AutoSize = true;
            this.cbFechaInicial.Enabled = false;
            this.cbFechaInicial.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFechaInicial.Location = new System.Drawing.Point(523, 56);
            this.cbFechaInicial.Name = "cbFechaInicial";
            this.cbFechaInicial.Size = new System.Drawing.Size(125, 25);
            this.cbFechaInicial.TabIndex = 4;
            this.cbFechaInicial.Text = "Fecha Inicial";
            this.cbFechaInicial.UseVisualStyleBackColor = true;
            this.cbFechaInicial.Visible = false;
            this.cbFechaInicial.Click += new System.EventHandler(this.CbFechaInicial_Click);
            // 
            // panelRangoFin
            // 
            this.panelRangoFin.Controls.Add(this.cboxMesRangoFin);
            this.panelRangoFin.Controls.Add(this.txbYearRangoFin);
            this.panelRangoFin.Controls.Add(this.txbDiaRangoFin);
            this.panelRangoFin.Controls.Add(this.label7);
            this.panelRangoFin.Controls.Add(this.label8);
            this.panelRangoFin.Controls.Add(this.label9);
            this.panelRangoFin.Enabled = false;
            this.panelRangoFin.Location = new System.Drawing.Point(107, 206);
            this.panelRangoFin.Name = "panelRangoFin";
            this.panelRangoFin.Size = new System.Drawing.Size(292, 151);
            this.panelRangoFin.TabIndex = 3;
            // 
            // cboxMesRangoFin
            // 
            this.cboxMesRangoFin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboxMesRangoFin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboxMesRangoFin.Enabled = false;
            this.cboxMesRangoFin.FormattingEnabled = true;
            this.cboxMesRangoFin.Items.AddRange(new object[] {
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"});
            this.cboxMesRangoFin.Location = new System.Drawing.Point(136, 61);
            this.cboxMesRangoFin.Name = "cboxMesRangoFin";
            this.cboxMesRangoFin.Size = new System.Drawing.Size(121, 24);
            this.cboxMesRangoFin.TabIndex = 14;
            this.cboxMesRangoFin.SelectedIndexChanged += new System.EventHandler(this.CboxMesRangoFin_SelectedIndexChanged);
            this.cboxMesRangoFin.TextChanged += new System.EventHandler(this.cboxMesRangoFin_TextChanged);
            // 
            // txbYearRangoFin
            // 
            this.txbYearRangoFin.Enabled = false;
            this.txbYearRangoFin.Location = new System.Drawing.Point(136, 111);
            this.txbYearRangoFin.MaxLength = 4;
            this.txbYearRangoFin.Name = "txbYearRangoFin";
            this.txbYearRangoFin.Size = new System.Drawing.Size(100, 22);
            this.txbYearRangoFin.TabIndex = 13;
            this.txbYearRangoFin.TextChanged += new System.EventHandler(this.txbYearRangoFin_TextChanged);
            // 
            // txbDiaRangoFin
            // 
            this.txbDiaRangoFin.Enabled = false;
            this.txbDiaRangoFin.Location = new System.Drawing.Point(136, 21);
            this.txbDiaRangoFin.MaxLength = 2;
            this.txbDiaRangoFin.Name = "txbDiaRangoFin";
            this.txbDiaRangoFin.Size = new System.Drawing.Size(100, 22);
            this.txbDiaRangoFin.TabIndex = 12;
            this.txbDiaRangoFin.TextChanged += new System.EventHandler(this.txbDiaRangoFin_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Enabled = false;
            this.label7.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(36, 107);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 24);
            this.label7.TabIndex = 11;
            this.label7.Text = "Año";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Enabled = false;
            this.label8.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(36, 63);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 24);
            this.label8.TabIndex = 10;
            this.label8.Text = "Mes";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Enabled = false;
            this.label9.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(36, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 24);
            this.label9.TabIndex = 9;
            this.label9.Text = "Día";
            // 
            // panelRangoInicio
            // 
            this.panelRangoInicio.Controls.Add(this.cboxMesRangoInicio);
            this.panelRangoInicio.Controls.Add(this.txbYearRangoInicio);
            this.panelRangoInicio.Controls.Add(this.txbDiaRangoInicio);
            this.panelRangoInicio.Controls.Add(this.label4);
            this.panelRangoInicio.Controls.Add(this.label5);
            this.panelRangoInicio.Controls.Add(this.label6);
            this.panelRangoInicio.Enabled = false;
            this.panelRangoInicio.Location = new System.Drawing.Point(107, 34);
            this.panelRangoInicio.Name = "panelRangoInicio";
            this.panelRangoInicio.Size = new System.Drawing.Size(292, 151);
            this.panelRangoInicio.TabIndex = 2;
            // 
            // cboxMesRangoInicio
            // 
            this.cboxMesRangoInicio.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboxMesRangoInicio.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboxMesRangoInicio.Enabled = false;
            this.cboxMesRangoInicio.FormattingEnabled = true;
            this.cboxMesRangoInicio.Items.AddRange(new object[] {
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"});
            this.cboxMesRangoInicio.Location = new System.Drawing.Point(136, 61);
            this.cboxMesRangoInicio.Name = "cboxMesRangoInicio";
            this.cboxMesRangoInicio.Size = new System.Drawing.Size(121, 24);
            this.cboxMesRangoInicio.TabIndex = 14;
            this.cboxMesRangoInicio.TextChanged += new System.EventHandler(this.cboxMesRangoInicio_TextChanged);
            // 
            // txbYearRangoInicio
            // 
            this.txbYearRangoInicio.Enabled = false;
            this.txbYearRangoInicio.Location = new System.Drawing.Point(136, 111);
            this.txbYearRangoInicio.MaxLength = 4;
            this.txbYearRangoInicio.Name = "txbYearRangoInicio";
            this.txbYearRangoInicio.Size = new System.Drawing.Size(100, 22);
            this.txbYearRangoInicio.TabIndex = 13;
            this.txbYearRangoInicio.TextChanged += new System.EventHandler(this.txbYearRangoInicio_TextChanged);
            // 
            // txbDiaRangoInicio
            // 
            this.txbDiaRangoInicio.Enabled = false;
            this.txbDiaRangoInicio.Location = new System.Drawing.Point(136, 21);
            this.txbDiaRangoInicio.MaxLength = 2;
            this.txbDiaRangoInicio.Name = "txbDiaRangoInicio";
            this.txbDiaRangoInicio.Size = new System.Drawing.Size(100, 22);
            this.txbDiaRangoInicio.TabIndex = 12;
            this.txbDiaRangoInicio.TextChanged += new System.EventHandler(this.txbDiaRangoInicio_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Enabled = false;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(36, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 24);
            this.label4.TabIndex = 11;
            this.label4.Text = "Año";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Enabled = false;
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(36, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 24);
            this.label5.TabIndex = 10;
            this.label5.Text = "Mes";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Enabled = false;
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(36, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 24);
            this.label6.TabIndex = 9;
            this.label6.Text = "Día";
            // 
            // calendarRango
            // 
            this.calendarRango.Enabled = false;
            this.calendarRango.Location = new System.Drawing.Point(523, 89);
            this.calendarRango.Name = "calendarRango";
            this.calendarRango.TabIndex = 1;
            this.calendarRango.Visible = false;
            this.calendarRango.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.calendarRango_DateChanged);
            // 
            // cbRango
            // 
            this.cbRango.AutoSize = true;
            this.cbRango.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbRango.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbRango.Location = new System.Drawing.Point(0, 0);
            this.cbRango.Name = "cbRango";
            this.cbRango.Size = new System.Drawing.Size(838, 28);
            this.cbRango.TabIndex = 0;
            this.cbRango.Text = "Rango";
            this.cbRango.UseVisualStyleBackColor = true;
            this.cbRango.CheckedChanged += new System.EventHandler(this.cbRango_CheckedChanged);
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Location = new System.Drawing.Point(391, 672);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(120, 41);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // formCalendario
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(867, 736);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.panelRango);
            this.Controls.Add(this.panelFecha);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formCalendario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calendario";
            this.Load += new System.EventHandler(this.formCalendario_Load);
            this.panelFecha.ResumeLayout(false);
            this.panelFecha.PerformLayout();
            this.panelRango.ResumeLayout(false);
            this.panelRango.PerformLayout();
            this.panelRangoFin.ResumeLayout(false);
            this.panelRangoFin.PerformLayout();
            this.panelRangoInicio.ResumeLayout(false);
            this.panelRangoInicio.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelFecha;
        private System.Windows.Forms.Panel panelRango;
        private System.Windows.Forms.CheckBox cbFecha;
        private System.Windows.Forms.CheckBox cbRango;
        private System.Windows.Forms.MonthCalendar calendarFecha;
        private System.Windows.Forms.TextBox txbYearFecha;
        private System.Windows.Forms.TextBox txbDiaFecha;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboxMesFecha;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.MonthCalendar calendarRango;
        private System.Windows.Forms.Panel panelRangoFin;
        private System.Windows.Forms.Panel panelRangoInicio;
        private System.Windows.Forms.ComboBox cboxMesRangoInicio;
        private System.Windows.Forms.TextBox txbYearRangoInicio;
        private System.Windows.Forms.TextBox txbDiaRangoInicio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboxMesRangoFin;
        private System.Windows.Forms.TextBox txbYearRangoFin;
        private System.Windows.Forms.TextBox txbDiaRangoFin;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox cbFechaInicial;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.CheckBox cbFechaFinal;
    }
}