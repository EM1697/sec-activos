﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql;
using MySql.Data.MySqlClient;
using Excel = Microsoft.Office.Interop.Excel;

namespace SEC_Activos
{
    public partial class formExportar : Form
    {
        #region Form

        public formExportar(string user = "")
        {
            InitializeComponent();
            this.user = "Usuario: " + user;
        }

        private void formExportar_Load(object sender, EventArgs e)
        {
            btnExportar.Left = this.Size.Width / 2 - (btnExportar.Size.Width / 2);
            tablePreview.Width = this.Width - (this.Width / 10);
            tablePreview.Left = this.Size.Width / 20;
            panelContent.Width = this.Width - (this.Width / 10);
            labelInfo.Text = todoInfo;
            panelContent.Hide();
            toolTip1.ShowAlways = true;
            toolTip1.SetToolTip(txbFecha, "Haz click para ingresar fecha.");
            LlenarTabla();
            labelUser.Text = user;
        }

        private void formExportar_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        #endregion

        #region Variables

        string todoInfo = "Esta opción genera archivo de Excel de todos los datos. Dar click en \"Exportar\".";
        string filtrosInfo = "Seleccionar filtro:";
        bool todoClicked = true;
        bool filtrosClicked = false;
        public string fecha = "";
        public string user = "";
        MySqlConnection conn = new MySqlConnection("SERVER=localhost;DATABASE=sec_activos;UID=EMM;PASSWORD=ACDC;");
        string nombreFiltros = "";
        bool ceroRegistros = true;

        #endregion

        #region Menu

        private void menuOptionTodo_Click(object sender, EventArgs e)
        {
            if (!todoClicked)
                LlenarTabla();
            panelCantidad.Show();
            menuOptionTodo.BackColor = SystemColors.ControlDark;
            menuOptionFiltros.BackColor = SystemColors.ControlLight;
            menuOptionSesion.BackColor = SystemColors.ControlLight;
            labelInfo.Text = todoInfo;
            panelContent.Hide();
            if (!todoClicked)
            {
                tablePreview.Location = new Point(tablePreview.Location.X, tablePreview.Location.Y - 100);
                tablePreview.Height += 100;
            }
            todoClicked = true;
            filtrosClicked = false;
        }

        private void menuOptionFiltros_Click(object sender, EventArgs e)
        {
            panelCantidad.Hide();
            tablePreview.DataSource = null;
            tablePreview.Refresh();
            menuOptionTodo.BackColor = SystemColors.ControlLight;
            menuOptionFiltros.BackColor = SystemColors.ControlDark;
            menuOptionSesion.BackColor = SystemColors.ControlLight;
            labelInfo.Text = filtrosInfo;
            panelContent.Show();
            LlenarComboDesc();
            if (!filtrosClicked)
            {
                tablePreview.Location = new Point(tablePreview.Location.X, tablePreview.Location.Y + 100);
                tablePreview.Height -= 100;
            }
            todoClicked = false;
            filtrosClicked = true;
            ceroRegistros = true;
        }

        private void menuOptionSesion_Click(object sender, EventArgs e)
        {
            menuOptionTodo.BackColor = SystemColors.ControlLight;
            menuOptionFiltros.BackColor = SystemColors.ControlLight;
            menuOptionSesion.BackColor = SystemColors.ControlDark;
            Hide();
            new formLogIn().Show();
        }

        #endregion

        #region CheckBox

        private void CbPartida_CheckedChanged_1(object sender, EventArgs e)
        {
            if (cbPartida.Checked)
            {
                txbPartida.Enabled = true;
            }
            else
            {
                txbPartida.Enabled = false;
            }
        }

        private void CbDesc_CheckedChanged(object sender, EventArgs e)
        {
            if (cbDesc.Checked)
            {
                cboxDesc.Enabled = true;
            }
            else
            {
                cboxDesc.Enabled = false;
            }
        }

        private void cbPartida_CheckedChanged(object sender, EventArgs e)
        {
            if (cbClaseAct.Checked)
            {
                txbClaseAct.Enabled = true;
            }
            else
            {
                txbClaseAct.Enabled = false;
            }
        }

        private void cbCuenta_CheckedChanged(object sender, EventArgs e)
        {
            if (cbCuenta.Checked)
            {
                txbCuenta.Enabled = true;
            }
            else
            {
                txbCuenta.Enabled = false;
            }
        }

        private void cbSubcuenta_CheckedChanged(object sender, EventArgs e)
        {
            if (cbSubcuenta.Checked)
            {
                txbSubcuenta.Enabled = true;
            }
            else
            {
                txbSubcuenta.Enabled = false;
            }
        }

        private void cbFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (cbFecha.Checked)
            {
                txbFecha.Enabled = true;
                if (txbFecha.Text == "")
                    txbFecha.Text = "Haz click";
            }
            else
            {
                txbFecha.Enabled = false;
                if (txbFecha.Text == "Haz click")
                    txbFecha.Text = "";
            }
        }

        private void CbSociedad_CheckedChanged(object sender, EventArgs e)
        {
            if (cbSociedad.Checked)
            {
                txbSociedad.Enabled = true;
            }
            else
            {
                txbSociedad.Enabled = false;
            }
        }

        #endregion

        #region TextBox

        private void TxbPartida_TextChanged_1(object sender, EventArgs e)
        {
            try
            {
                if (txbCuenta.Text.Length == 1)
                {
                    char letter = txbCuenta.Text[0];
                    if (!Char.IsDigit(letter))
                    {
                        txbCuenta.Text = txbCuenta.Text.Substring(0, 0);
                    }
                }
                else if (txbCuenta.Text.Length > 1)
                {
                    char letter = txbCuenta.Text[txbCuenta.Text.Length - 1];
                    if (!Char.IsDigit(letter))
                    {
                        txbCuenta.Text = txbCuenta.Text.Substring(0, txbCuenta.Text.Length - 1);
                    }
                    txbCuenta.SelectionStart = txbCuenta.Text.Length;
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(this, ex.Message, "Partida");
            }
        }

        private void txbSociedad_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txbSociedad.Text.Length == 1)
                {
                    char letter = txbSociedad.Text[0];
                    if (!Char.IsDigit(letter))
                    {
                        txbSociedad.Text = txbSociedad.Text.Substring(0, 0);
                    }
                }
                else if (txbSociedad.Text.Length > 1)
                {
                    char letter = txbSociedad.Text[txbSociedad.Text.Length - 1];
                    if (!Char.IsDigit(letter))
                    {
                        txbSociedad.Text = txbSociedad.Text.Substring(0, txbSociedad.Text.Length - 1);
                    }
                    txbSociedad.SelectionStart = txbSociedad.Text.Length;
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(this, ex.Message, "Sociedad");
            }
        }

        private void txbClaseAct_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txbClaseAct.Text.Length == 1)
                {
                    char letter = txbClaseAct.Text[0];
                    if (!Char.IsDigit(letter))
                    {
                        txbClaseAct.Text = txbClaseAct.Text.Substring(0, 0);
                    }
                }
                else if (txbClaseAct.Text.Length > 1)
                {
                    char letter = txbClaseAct.Text[txbClaseAct.Text.Length - 1];
                    if (!Char.IsDigit(letter))
                    {
                        txbClaseAct.Text = txbClaseAct.Text.Substring(0, txbClaseAct.Text.Length - 1);
                    }
                    txbClaseAct.SelectionStart = txbClaseAct.Text.Length;
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(this, ex.Message, "Clase Activo");
            }
        }

        private void txbCuenta_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txbCuenta.Text.Length == 1)
                {
                    char letter = txbCuenta.Text[0];
                    if (!Char.IsDigit(letter))
                    {
                        txbCuenta.Text = txbCuenta.Text.Substring(0, 0);
                    }
                }
                else if (txbCuenta.Text.Length > 1)
                {
                    char letter = txbCuenta.Text[txbCuenta.Text.Length - 1];
                    if (!Char.IsDigit(letter))
                    {
                        txbCuenta.Text = txbCuenta.Text.Substring(0, txbCuenta.Text.Length - 1);
                    }
                    txbCuenta.SelectionStart = txbCuenta.Text.Length;
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(this, ex.Message, "Cuenta");
            }
        }

        private void txbSubcuenta_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txbSubcuenta.Text.Length == 1)
                {
                    char letter = txbSubcuenta.Text[0];
                    if (!Char.IsDigit(letter))
                    {
                        txbSubcuenta.Text = txbSubcuenta.Text.Substring(0, 0);
                    }
                }
                else if (txbSubcuenta.Text.Length > 1)
                {
                    char letter = txbSubcuenta.Text[txbSubcuenta.Text.Length - 1];
                    if (!Char.IsDigit(letter))
                    {
                        txbSubcuenta.Text = txbSubcuenta.Text.Substring(0, txbSubcuenta.Text.Length - 1);
                    }
                    txbSubcuenta.SelectionStart = txbSubcuenta.Text.Length;
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(this, ex.Message, "Subcuenta");
            }
        }

        private void txbFecha_Click(object sender, EventArgs e)
        {
            if (cbFecha.Checked)
            {
                using (formCalendario cal = new formCalendario() { calendario = new Calendar() })
                {
                    if (cal.ShowDialog() == DialogResult.OK || cal.val == true)
                        txbFecha.Text = cal.fecha;
                }
            }
        }

        private void txbFecha_TextChanged(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txbFecha, txbFecha.Text);
        }

        #endregion

        #region Button

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            txbClaseAct.Text = null;
            txbCuenta.Text = null;
            txbFecha.Text = null;
            txbSociedad.Text = null;
            txbSubcuenta.Text = null;
        }

        private void BtnExportar_Click(object sender, EventArgs e)
        {
            if ((!cbSociedad.Checked && !cbConciliado.Checked && !cbClaseAct.Checked && !cbFecha.Checked && !cbCuenta.Checked && !cbSubcuenta.Checked && !cbPartida.Checked && !cbDesc.Checked) && filtrosClicked)
                MessageBox.Show(this, "Debe seleccionar al menos un campo.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                if (filtrosClicked && ceroRegistros)
                    RealizarFiltros();
                if (!ceroRegistros)
                    CrearExcel();
            }
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            if (!cbSociedad.Checked && !cbConciliado.Checked && !cbClaseAct.Checked && !cbFecha.Checked && !cbCuenta.Checked && !cbSubcuenta.Checked && !cbPartida.Checked && !cbDesc.Checked)
                MessageBox.Show(this, "Debe seleccionar al menos un campo.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
                RealizarFiltros();
        }

        #endregion

        #region Methods
        private void LlenarComboDesc()
        {
            try
            {
                string query = "SELECT DISTINCT(A_DESC) FROM activos where A_DESC is not null GROUP BY A_DESC";
                conn.Open();
                using (var command = new MySqlCommand(query, conn))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        //Iterate through the rows and add it to the combobox's items
                        while (reader.Read())
                        {
                            cboxDesc.Items.Add(reader.GetString("A_DESC"));
                        }
                    }
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                // write exception info to log or anything else
                MessageBox.Show("Error en Combo Desc");
                conn.Close();
            }
        }

        private void LlenarTabla(string query = "SELECT * FROM activos")
        {
            try
            {
                conn.Open();

                #region Table
                DataTable dt = new DataTable();
                MySqlCommand cmd = new MySqlCommand(query, conn);
                MySqlDataReader reader = cmd.ExecuteReader();
                dt.Load(reader);
                if (dt.Rows.Count > 0)
                {
                    tablePreview.DataSource = dt;
                    ceroRegistros = false;
                }
                else
                {
                    ceroRegistros = true;
                    MessageBox.Show(this, "No se encontraron resultados con los criterios ingresados o la tabla está vacía.", "Ningún resultado", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                #endregion

                #region Labels

                int count = -1;
                string select = query.Substring(0, 6);
                string body = query.Substring(8);
                query = select + " COUNT(*)" + body;
                cmd = new MySqlCommand(query, conn);
                count = int.Parse(cmd.ExecuteScalar() + "");
                panelCantidad.Show();
                labelTotalRegistros.Text = count.ToString();

                #endregion

                conn.Close();
            }
            catch (Exception e)
            {
                conn.Close();
                MessageBox.Show(this, e.Message, "Llenar Tabla");
            }
        }

        private void CrearExcel()
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xlsx";
            if (todoClicked)
            {
                sfd.FileName = "Reporte_Activos_Completo_" + DateTime.Now.ToString() + ".xls";
                sfd.FileName = sfd.FileName.Replace(':', '.');
            }
            else if (filtrosClicked)
            {
                sfd.FileName = "Reporte_Activos_" + nombreFiltros + ".xls";
            }
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                CopyAlltoClipboard();

                object misValue = System.Reflection.Missing.Value;
                Excel.Application xlexcel = new Excel.Application();
                xlexcel.DisplayAlerts = false; // Without this you will get two confirm overwrite prompts
                Excel.Workbook xlWorkBook = xlexcel.Workbooks.Add(misValue);
                Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                //Headers
                for (int i = 1; i < tablePreview.Columns.Count + 1; i++)
                {
                    xlWorkSheet.Cells[1, i] = tablePreview.Columns[i - 1].HeaderText;
                }

                //Freeze First Row
                FreezeFirstRow(xlWorkSheet);

                // Paste clipboard results to worksheet range
                Excel.Range CR = (Excel.Range)xlWorkSheet.Cells[2, 1];
                CR.Select();
                xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);

                // Delete blank column A and select cell A2
                Excel.Range delRng = xlWorkSheet.get_Range("A:A").Cells;
                delRng.Delete(Type.Missing);
                xlWorkSheet.get_Range("A2").Select();

                // Save the excel file under the captured location from the SaveFileDialog
                xlWorkBook.SaveAs(sfd.FileName, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlexcel.DisplayAlerts = true;
                xlWorkBook.Close(true, misValue, misValue);
                xlexcel.Quit();

                ReleaseObject(xlWorkSheet);
                ReleaseObject(xlWorkBook);
                ReleaseObject(xlexcel);

                // Clear Clipboard and DataGridView selection
                Clipboard.Clear();
                tablePreview.ClearSelection();

                // Open the newly saved excel file
                if (File.Exists(sfd.FileName))
                    System.Diagnostics.Process.Start(sfd.FileName);
            }
        }

        private void CopyAlltoClipboard()
        {
            tablePreview.SelectAll();
            DataObject dataObj = tablePreview.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        private void FreezeFirstRow(Excel.Worksheet workSheet)
        {
            // Fix first row
            workSheet.Activate();
            workSheet.Application.ActiveWindow.SplitRow = 1;
            workSheet.Application.ActiveWindow.FreezePanes = true;
            // Now apply autofilter
            Excel.Range firstRow = (Excel.Range)workSheet.Rows[1];
            firstRow.AutoFilter(1,
                                Type.Missing,
                                Excel.XlAutoFilterOperator.xlAnd,
                                Type.Missing,
                                true);
        }

        private void ReleaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occurred while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void RealizarFiltros()
        {
            tablePreview.DataSource = null;
            tablePreview.Refresh();
            string query = "SELECT * from ACTIVOS where";
            List<string> listaCB = RecorrerCheckBox();
            int total = listaCB.Count();
            int counter = 0;
            bool empty = false;
            nombreFiltros = "";
            foreach (string cb in listaCB)
            {
                counter++;
                switch (cb)
                {
                    #region Sociedad
                    case "Sociedad":
                        if (txbSociedad.Text == "")
                        {
                            MessageBox.Show(this, "Agregar información de SOCIEDAD para realizar búsqueda.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            empty = !empty;
                            break;
                        }
                        else if (counter == total)
                        {
                            query += " A_SOCIEDAD = " + txbSociedad.Text;
                        }
                        else
                        {
                            query += " A_SOCIEDAD = " + txbSociedad.Text + " AND";
                        }
                        nombreFiltros += "_Sociedad=" + txbSociedad.Text;
                        break;
                    #endregion

                    #region Conciliado
                    case "Conciliado (Si/No)":
                        if (counter == total)
                        {
                            query += " A_ERROR = 'CONCI'";
                        }
                        else
                        {
                            query += " A_ERROR = 'CONCI' AND";
                        }
                        nombreFiltros += "_Conciliado=Si";
                        break;
                    #endregion

                    #region Clase Activo
                    case "Clase Activo":
                        if (txbClaseAct.Text == "")
                        {
                            MessageBox.Show(this, "Agregar información de CLASE ACTIVO para realizar búsqueda.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            empty = !empty;
                            break;
                        }
                        else if (counter == total)
                        {
                            query += " A_CLASE_ACTIVO = " + txbClaseAct.Text;
                        }
                        else
                        {
                            query += " A_CLASE_ACTIVO = " + txbClaseAct.Text + " AND";
                        }
                        nombreFiltros += "_ClaseActivo=" + txbClaseAct.Text;
                        break;
                    #endregion

                    #region Fecha
                    case "Fecha de entidad":
                        string fechaCompleta = txbFecha.Text;
                        if (txbFecha.Text == "Haz click")
                        {
                            MessageBox.Show(this, "Agregar información de FECHA DE ENTIDAD para realizar búsqueda.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            empty = !empty;
                            break;
                        }
                        else if (fechaCompleta.Contains('-'))
                        {
                            string fechaInicio = fechaCompleta.Substring(0, fechaCompleta.IndexOf('-')-1);
                            string fechaFin = fechaCompleta.Substring(fechaCompleta.IndexOf('-') + 2);
                            if (counter == total)
                            {
                                query += " A_FEENT >= " + DateForDatabase(fechaInicio) + " AND A_FEENT <= " + DateForDatabase(fechaFin) ;
                            }
                            else
                            {
                                query += " A_FEENT >= " + DateForDatabase(fechaInicio) + " AND A_FEENT <= " + DateForDatabase(fechaFin) + " AND";
                            }
                        }
                        else
                        {
                            if (counter == total)
                            {
                                query += " A_FEENT = " + DateForDatabase(fechaCompleta);
                            }
                            else
                            {
                                query += " A_FEENT = " + DateForDatabase(fechaCompleta) + " AND";
                            }
                        }
                        nombreFiltros += "_FechaEntidad=" + txbFecha.Text;
                        break;
                    #endregion

                    #region Cuenta
                    case "Cuenta":
                        if (txbCuenta.Text == "")
                        {
                            MessageBox.Show(this, "Agregar información de CUENTA para realizar búsqueda.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            empty = !empty;
                            break;
                        }
                        else if (counter == total)
                        {
                            query += " A_CTA = " + txbCuenta.Text;
                        }
                        else
                        {
                            query += " A_CTA = " + txbCuenta.Text + " AND";
                        }
                        nombreFiltros += "_Cuenta=" + txbCuenta.Text;
                        break;
                    #endregion

                    #region Subcuenta
                    case "Subcuenta":
                        if (txbSubcuenta.Text == "")
                        {
                            MessageBox.Show(this, "Agregar información de SUBCUENTA para realizar búsqueda.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            empty = !empty;
                            break;
                        }
                        else if (counter == total)
                        {
                            query += " A_SUBCTA = " + txbSubcuenta.Text;
                        }
                        else
                        {
                            query += " A_SUBCTA = " + txbSubcuenta.Text + " AND";
                        }
                        nombreFiltros += "_Subcuenta=" + txbSubcuenta.Text;
                        break;
                    #endregion

                    #region Partida
                    case "Partida":
                        if (txbPartida.Text == "")
                        {
                            MessageBox.Show(this, "Agregar información de PARTIDA para realizar búsqueda.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            empty = !empty;
                            break;
                        }
                        else if (counter == total)
                        {
                            query += " A_PART = " + txbPartida.Text;
                        }
                        else
                        {
                            query += " A_PART = " + txbPartida.Text + " AND";
                        }
                        nombreFiltros += "_Partida=" + txbPartida.Text;
                        break;
                    #endregion

                    #region Descripción Activo
                    case "Descripción Activo":
                        if (cboxDesc.Text == "")
                        {
                            MessageBox.Show(this, "Seleccione información de DESCRIPCIÓN ACTIVO para realizar búsqueda.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            empty = !empty;
                            break;
                        }
                        else if (counter == total)
                        {
                            query += " A_DESC = '" + cboxDesc.Text + "'";
                        }
                        else
                        {
                            query += " A_DESC = '" + cboxDesc.Text + "' AND";
                        }
                        nombreFiltros += "_Activo=" + cboxDesc.Text;
                        break;
                        #endregion
                }
                if (empty)
                    break;
            }
            if(!empty)
                LlenarTabla(query);
        }

        private List<string> RecorrerCheckBox()
        {
            List<string> list = new List<string>();
            foreach (CheckBox cb in panelContent.Controls.OfType<CheckBox>())
            {
                if (cb.Checked == true)
                    list.Add(cb.Text);
            }
            return list;
        }

        private string DateForDatabase(string date = "")
        {
            if (date == "")
            {
                MessageBox.Show(this, "Fecha Incorrecta", "DateForDatabase", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                string dia = date.Substring(0, date.IndexOf('/'));
                string mes = date.Substring(date.IndexOf('/') + 1, date.IndexOf('/', date.IndexOf('/') + 1) - date.IndexOf('/') - 1);
                string year = date.Substring(date.Length - 4);
                date = "'" + year + "-" + Mes(mes) + "-" + dia + "'";
            }
            return date;
        }

        public string Mes(string mes)
        {
            switch (mes)
            {
                case "Enero":
                    return "01";
                case "Febrero":
                    return "02";
                case "Marzo":
                    return "03";
                case "Abril":
                    return "04";
                case "Mayo":
                    return "05";
                case "Junio":
                    return "06";
                case "Julio":
                    return "07";
                case "Agosto":
                    return "08";
                case "Septiembre":
                    return "09";
                case "Octubre":
                    return "10";
                case "Noviembre":
                    return "11";
                case "Diciembre":
                    return "12";
                default:
                    return "";
            }
        }

        #endregion

    }
}
