﻿namespace SEC_Activos
{
    partial class formExportar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formExportar));
            this.menuPrincipal = new System.Windows.Forms.MenuStrip();
            this.menuOptionTodo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOptionFiltros = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOptionSesion = new System.Windows.Forms.ToolStripMenuItem();
            this.tablePreview = new System.Windows.Forms.DataGridView();
            this.btnExportar = new System.Windows.Forms.Button();
            this.panelContent = new System.Windows.Forms.Panel();
            this.txbPartida = new System.Windows.Forms.TextBox();
            this.cbPartida = new System.Windows.Forms.CheckBox();
            this.cboxDesc = new System.Windows.Forms.ComboBox();
            this.cbDesc = new System.Windows.Forms.CheckBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txbFecha = new System.Windows.Forms.TextBox();
            this.txbSubcuenta = new System.Windows.Forms.TextBox();
            this.txbCuenta = new System.Windows.Forms.TextBox();
            this.txbClaseAct = new System.Windows.Forms.TextBox();
            this.txbSociedad = new System.Windows.Forms.TextBox();
            this.cbSubcuenta = new System.Windows.Forms.CheckBox();
            this.cbCuenta = new System.Windows.Forms.CheckBox();
            this.cbFecha = new System.Windows.Forms.CheckBox();
            this.cbClaseAct = new System.Windows.Forms.CheckBox();
            this.cbConciliado = new System.Windows.Forms.CheckBox();
            this.cbSociedad = new System.Windows.Forms.CheckBox();
            this.labelInfo = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panelCantidad = new System.Windows.Forms.Panel();
            this.labelTotalRegistros = new System.Windows.Forms.Label();
            this.labelCantidadRegistros = new System.Windows.Forms.Label();
            this.labelUser = new System.Windows.Forms.Label();
            this.menuPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tablePreview)).BeginInit();
            this.panelContent.SuspendLayout();
            this.panelCantidad.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuPrincipal
            // 
            this.menuPrincipal.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuOptionTodo,
            this.menuOptionFiltros,
            this.menuOptionSesion});
            this.menuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.menuPrincipal.Name = "menuPrincipal";
            this.menuPrincipal.Padding = new System.Windows.Forms.Padding(5, 3, 0, 3);
            this.menuPrincipal.Size = new System.Drawing.Size(1174, 31);
            this.menuPrincipal.TabIndex = 0;
            this.menuPrincipal.Text = "Menú Principal";
            // 
            // menuOptionTodo
            // 
            this.menuOptionTodo.BackColor = System.Drawing.SystemColors.ControlDark;
            this.menuOptionTodo.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuOptionTodo.Name = "menuOptionTodo";
            this.menuOptionTodo.Size = new System.Drawing.Size(140, 25);
            this.menuOptionTodo.Text = "Exportar todo";
            this.menuOptionTodo.Click += new System.EventHandler(this.menuOptionTodo_Click);
            // 
            // menuOptionFiltros
            // 
            this.menuOptionFiltros.BackColor = System.Drawing.SystemColors.Control;
            this.menuOptionFiltros.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuOptionFiltros.Name = "menuOptionFiltros";
            this.menuOptionFiltros.Size = new System.Drawing.Size(76, 25);
            this.menuOptionFiltros.Text = "Filtros";
            this.menuOptionFiltros.Click += new System.EventHandler(this.menuOptionFiltros_Click);
            // 
            // menuOptionSesion
            // 
            this.menuOptionSesion.BackColor = System.Drawing.SystemColors.ControlLight;
            this.menuOptionSesion.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuOptionSesion.Name = "menuOptionSesion";
            this.menuOptionSesion.Size = new System.Drawing.Size(136, 25);
            this.menuOptionSesion.Text = "Cerrar Sesión";
            this.menuOptionSesion.Click += new System.EventHandler(this.menuOptionSesion_Click);
            // 
            // tablePreview
            // 
            this.tablePreview.AllowUserToAddRows = false;
            this.tablePreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tablePreview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tablePreview.Location = new System.Drawing.Point(183, 129);
            this.tablePreview.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tablePreview.Name = "tablePreview";
            this.tablePreview.RowTemplate.Height = 24;
            this.tablePreview.Size = new System.Drawing.Size(841, 402);
            this.tablePreview.TabIndex = 2;
            // 
            // btnExportar
            // 
            this.btnExportar.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.btnExportar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnExportar.BackColor = System.Drawing.Color.Gray;
            this.btnExportar.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExportar.Location = new System.Drawing.Point(543, 553);
            this.btnExportar.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(132, 57);
            this.btnExportar.TabIndex = 1;
            this.btnExportar.Text = "Exportar";
            this.btnExportar.UseVisualStyleBackColor = false;
            this.btnExportar.Click += new System.EventHandler(this.BtnExportar_Click);
            // 
            // panelContent
            // 
            this.panelContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelContent.Controls.Add(this.txbPartida);
            this.panelContent.Controls.Add(this.cbPartida);
            this.panelContent.Controls.Add(this.cboxDesc);
            this.panelContent.Controls.Add(this.cbDesc);
            this.panelContent.Controls.Add(this.btnRefresh);
            this.panelContent.Controls.Add(this.btnBuscar);
            this.panelContent.Controls.Add(this.txbFecha);
            this.panelContent.Controls.Add(this.txbSubcuenta);
            this.panelContent.Controls.Add(this.txbCuenta);
            this.panelContent.Controls.Add(this.txbClaseAct);
            this.panelContent.Controls.Add(this.txbSociedad);
            this.panelContent.Controls.Add(this.cbSubcuenta);
            this.panelContent.Controls.Add(this.cbCuenta);
            this.panelContent.Controls.Add(this.cbFecha);
            this.panelContent.Controls.Add(this.cbClaseAct);
            this.panelContent.Controls.Add(this.cbConciliado);
            this.panelContent.Controls.Add(this.cbSociedad);
            this.panelContent.Location = new System.Drawing.Point(25, 67);
            this.panelContent.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panelContent.Name = "panelContent";
            this.panelContent.Size = new System.Drawing.Size(1136, 151);
            this.panelContent.TabIndex = 3;
            // 
            // txbPartida
            // 
            this.txbPartida.Enabled = false;
            this.txbPartida.Location = new System.Drawing.Point(866, 22);
            this.txbPartida.MaxLength = 20;
            this.txbPartida.Name = "txbPartida";
            this.txbPartida.Size = new System.Drawing.Size(100, 26);
            this.txbPartida.TabIndex = 16;
            this.txbPartida.TextChanged += new System.EventHandler(this.TxbPartida_TextChanged_1);
            // 
            // cbPartida
            // 
            this.cbPartida.AutoSize = true;
            this.cbPartida.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPartida.Location = new System.Drawing.Point(735, 19);
            this.cbPartida.Name = "cbPartida";
            this.cbPartida.Size = new System.Drawing.Size(95, 28);
            this.cbPartida.TabIndex = 15;
            this.cbPartida.Text = "Partida";
            this.cbPartida.UseVisualStyleBackColor = true;
            this.cbPartida.CheckedChanged += new System.EventHandler(this.CbPartida_CheckedChanged_1);
            // 
            // cboxDesc
            // 
            this.cboxDesc.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboxDesc.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboxDesc.Enabled = false;
            this.cboxDesc.FormattingEnabled = true;
            this.cboxDesc.Location = new System.Drawing.Point(685, 110);
            this.cboxDesc.Name = "cboxDesc";
            this.cboxDesc.Size = new System.Drawing.Size(351, 26);
            this.cboxDesc.TabIndex = 14;
            // 
            // cbDesc
            // 
            this.cbDesc.AutoSize = true;
            this.cbDesc.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDesc.Location = new System.Drawing.Point(759, 76);
            this.cbDesc.Name = "cbDesc";
            this.cbDesc.Size = new System.Drawing.Size(194, 28);
            this.cbDesc.TabIndex = 13;
            this.cbDesc.Text = "Descripción Activo";
            this.cbDesc.UseVisualStyleBackColor = true;
            this.cbDesc.CheckedChanged += new System.EventHandler(this.CbDesc_CheckedChanged);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.Location = new System.Drawing.Point(1008, 55);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(49, 43);
            this.btnRefresh.TabIndex = 12;
            this.btnRefresh.Text = "";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.Location = new System.Drawing.Point(1008, 10);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(49, 40);
            this.btnBuscar.TabIndex = 11;
            this.btnBuscar.Text = "";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.BtnBuscar_Click);
            // 
            // txbFecha
            // 
            this.txbFecha.Enabled = false;
            this.txbFecha.ForeColor = System.Drawing.SystemColors.Info;
            this.txbFecha.Location = new System.Drawing.Point(241, 111);
            this.txbFecha.Name = "txbFecha";
            this.txbFecha.ReadOnly = true;
            this.txbFecha.Size = new System.Drawing.Size(100, 26);
            this.txbFecha.TabIndex = 9;
            this.txbFecha.Click += new System.EventHandler(this.txbFecha_Click);
            this.txbFecha.TextChanged += new System.EventHandler(this.txbFecha_TextChanged);
            // 
            // txbSubcuenta
            // 
            this.txbSubcuenta.Enabled = false;
            this.txbSubcuenta.Location = new System.Drawing.Point(519, 112);
            this.txbSubcuenta.MaxLength = 5;
            this.txbSubcuenta.Name = "txbSubcuenta";
            this.txbSubcuenta.Size = new System.Drawing.Size(100, 26);
            this.txbSubcuenta.TabIndex = 10;
            this.txbSubcuenta.TextChanged += new System.EventHandler(this.txbSubcuenta_TextChanged);
            // 
            // txbCuenta
            // 
            this.txbCuenta.Enabled = false;
            this.txbCuenta.Location = new System.Drawing.Point(519, 64);
            this.txbCuenta.MaxLength = 4;
            this.txbCuenta.Name = "txbCuenta";
            this.txbCuenta.Size = new System.Drawing.Size(100, 26);
            this.txbCuenta.TabIndex = 8;
            this.txbCuenta.TextChanged += new System.EventHandler(this.txbCuenta_TextChanged);
            // 
            // txbClaseAct
            // 
            this.txbClaseAct.Enabled = false;
            this.txbClaseAct.Location = new System.Drawing.Point(519, 19);
            this.txbClaseAct.MaxLength = 5;
            this.txbClaseAct.Name = "txbClaseAct";
            this.txbClaseAct.Size = new System.Drawing.Size(145, 26);
            this.txbClaseAct.TabIndex = 7;
            this.txbClaseAct.TextChanged += new System.EventHandler(this.txbClaseAct_TextChanged);
            // 
            // txbSociedad
            // 
            this.txbSociedad.Enabled = false;
            this.txbSociedad.Location = new System.Drawing.Point(168, 19);
            this.txbSociedad.MaxLength = 5;
            this.txbSociedad.Name = "txbSociedad";
            this.txbSociedad.Size = new System.Drawing.Size(100, 26);
            this.txbSociedad.TabIndex = 6;
            this.txbSociedad.TextChanged += new System.EventHandler(this.txbSociedad_TextChanged);
            // 
            // cbSubcuenta
            // 
            this.cbSubcuenta.AutoSize = true;
            this.cbSubcuenta.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSubcuenta.Location = new System.Drawing.Point(373, 109);
            this.cbSubcuenta.Name = "cbSubcuenta";
            this.cbSubcuenta.Size = new System.Drawing.Size(125, 28);
            this.cbSubcuenta.TabIndex = 5;
            this.cbSubcuenta.Text = "Subcuenta";
            this.cbSubcuenta.UseVisualStyleBackColor = true;
            this.cbSubcuenta.CheckedChanged += new System.EventHandler(this.cbSubcuenta_CheckedChanged);
            // 
            // cbCuenta
            // 
            this.cbCuenta.AutoSize = true;
            this.cbCuenta.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCuenta.Location = new System.Drawing.Point(373, 61);
            this.cbCuenta.Name = "cbCuenta";
            this.cbCuenta.Size = new System.Drawing.Size(95, 28);
            this.cbCuenta.TabIndex = 4;
            this.cbCuenta.Text = "Cuenta";
            this.cbCuenta.UseVisualStyleBackColor = true;
            this.cbCuenta.CheckedChanged += new System.EventHandler(this.cbCuenta_CheckedChanged);
            // 
            // cbFecha
            // 
            this.cbFecha.AutoSize = true;
            this.cbFecha.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFecha.Location = new System.Drawing.Point(50, 108);
            this.cbFecha.Name = "cbFecha";
            this.cbFecha.Size = new System.Drawing.Size(185, 28);
            this.cbFecha.TabIndex = 3;
            this.cbFecha.Text = "Fecha de entidad";
            this.cbFecha.UseVisualStyleBackColor = true;
            this.cbFecha.CheckedChanged += new System.EventHandler(this.cbFecha_CheckedChanged);
            // 
            // cbClaseAct
            // 
            this.cbClaseAct.AutoSize = true;
            this.cbClaseAct.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbClaseAct.Location = new System.Drawing.Point(373, 16);
            this.cbClaseAct.Name = "cbClaseAct";
            this.cbClaseAct.Size = new System.Drawing.Size(140, 28);
            this.cbClaseAct.TabIndex = 2;
            this.cbClaseAct.Text = "Clase Activo";
            this.cbClaseAct.UseVisualStyleBackColor = true;
            this.cbClaseAct.CheckedChanged += new System.EventHandler(this.cbPartida_CheckedChanged);
            // 
            // cbConciliado
            // 
            this.cbConciliado.AutoSize = true;
            this.cbConciliado.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbConciliado.Location = new System.Drawing.Point(50, 64);
            this.cbConciliado.Name = "cbConciliado";
            this.cbConciliado.Size = new System.Drawing.Size(193, 28);
            this.cbConciliado.TabIndex = 1;
            this.cbConciliado.Text = "Conciliado (Si/No)";
            this.cbConciliado.UseVisualStyleBackColor = true;
            // 
            // cbSociedad
            // 
            this.cbSociedad.AutoSize = true;
            this.cbSociedad.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSociedad.Location = new System.Drawing.Point(50, 16);
            this.cbSociedad.Name = "cbSociedad";
            this.cbSociedad.Size = new System.Drawing.Size(112, 28);
            this.cbSociedad.TabIndex = 0;
            this.cbSociedad.Text = "Sociedad";
            this.cbSociedad.UseVisualStyleBackColor = true;
            this.cbSociedad.CheckedChanged += new System.EventHandler(this.CbSociedad_CheckedChanged);
            // 
            // labelInfo
            // 
            this.labelInfo.AutoSize = true;
            this.labelInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfo.Location = new System.Drawing.Point(33, 44);
            this.labelInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(0, 32);
            this.labelInfo.TabIndex = 4;
            // 
            // panelCantidad
            // 
            this.panelCantidad.Controls.Add(this.labelTotalRegistros);
            this.panelCantidad.Controls.Add(this.labelCantidadRegistros);
            this.panelCantidad.Location = new System.Drawing.Point(760, 537);
            this.panelCantidad.Name = "panelCantidad";
            this.panelCantidad.Size = new System.Drawing.Size(322, 73);
            this.panelCantidad.TabIndex = 5;
            // 
            // labelTotalRegistros
            // 
            this.labelTotalRegistros.AutoSize = true;
            this.labelTotalRegistros.Location = new System.Drawing.Point(177, 27);
            this.labelTotalRegistros.Name = "labelTotalRegistros";
            this.labelTotalRegistros.Size = new System.Drawing.Size(64, 18);
            this.labelTotalRegistros.TabIndex = 1;
            this.labelTotalRegistros.Text = "Cantidad";
            // 
            // labelCantidadRegistros
            // 
            this.labelCantidadRegistros.AutoSize = true;
            this.labelCantidadRegistros.Location = new System.Drawing.Point(21, 27);
            this.labelCantidadRegistros.Name = "labelCantidadRegistros";
            this.labelCantidadRegistros.Size = new System.Drawing.Size(127, 18);
            this.labelCantidadRegistros.TabIndex = 0;
            this.labelCantidadRegistros.Text = "Total de registros:";
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.Dock = System.Windows.Forms.DockStyle.Right;
            this.labelUser.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUser.Location = new System.Drawing.Point(1174, 31);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(0, 24);
            this.labelUser.TabIndex = 2;
            // 
            // formExportar
            // 
            this.AcceptButton = this.btnExportar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(1174, 652);
            this.Controls.Add(this.labelUser);
            this.Controls.Add(this.panelCantidad);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.panelContent);
            this.Controls.Add(this.btnExportar);
            this.Controls.Add(this.tablePreview);
            this.Controls.Add(this.menuPrincipal);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuPrincipal;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(799, 669);
            this.Name = "formExportar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exportar";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.formExportar_FormClosed);
            this.Load += new System.EventHandler(this.formExportar_Load);
            this.menuPrincipal.ResumeLayout(false);
            this.menuPrincipal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tablePreview)).EndInit();
            this.panelContent.ResumeLayout(false);
            this.panelContent.PerformLayout();
            this.panelCantidad.ResumeLayout(false);
            this.panelCantidad.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuPrincipal;
        private System.Windows.Forms.ToolStripMenuItem menuOptionTodo;
        private System.Windows.Forms.ToolStripMenuItem menuOptionFiltros;
        private System.Windows.Forms.Button btnExportar;
        private System.Windows.Forms.DataGridView tablePreview;
        private System.Windows.Forms.Panel panelContent;
        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.ToolStripMenuItem menuOptionSesion;
        private System.Windows.Forms.TextBox txbSubcuenta;
        private System.Windows.Forms.TextBox txbCuenta;
        private System.Windows.Forms.TextBox txbClaseAct;
        private System.Windows.Forms.TextBox txbSociedad;
        private System.Windows.Forms.CheckBox cbSubcuenta;
        private System.Windows.Forms.CheckBox cbCuenta;
        private System.Windows.Forms.CheckBox cbFecha;
        private System.Windows.Forms.CheckBox cbClaseAct;
        private System.Windows.Forms.CheckBox cbConciliado;
        private System.Windows.Forms.CheckBox cbSociedad;
        private System.Windows.Forms.TextBox txbFecha;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panelCantidad;
        private System.Windows.Forms.Label labelTotalRegistros;
        private System.Windows.Forms.Label labelCantidadRegistros;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.ComboBox cboxDesc;
        private System.Windows.Forms.CheckBox cbDesc;
        private System.Windows.Forms.TextBox txbPartida;
        private System.Windows.Forms.CheckBox cbPartida;
    }
}