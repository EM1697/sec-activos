﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SEC_Activos
{
    public partial class formCalendario : Form
    {
        public formCalendario()
        {
            InitializeComponent();
        }

        #region Variables

        public string fecha = "";
        public Calendar calendario { get; set; }
        public bool val = false;
        DateTime fechaInicial;
        DateTime fechaFinal;

        #endregion

        private void cbFecha_CheckedChanged(object sender, EventArgs e)
        {
            if (cbFecha.Checked)
            {
                cbRango.Checked = false;
                cbRango.Enabled = true;
                cbFecha.Enabled = false;
                cbFechaInicial.Enabled = false;
                cbFechaFinal.Enabled = false;
                cbFechaInicial.Visible = false;
                cbFechaFinal.Visible = false;
                txbDiaFecha.Enabled = true;
                cboxMesFecha.Enabled = true;
                txbYearFecha.Enabled = true;
                txbDiaRangoInicio.Enabled = false;
                cboxMesRangoInicio.Enabled = false;
                txbYearRangoInicio.Enabled = false;
                txbDiaRangoFin.Enabled = false;
                cboxMesRangoFin.Enabled = false;
                txbYearRangoFin.Enabled = false;
                label1.Enabled = true;
                label2.Enabled = true;
                label3.Enabled = true;
                label4.Enabled = false;
                label5.Enabled = false;
                label6.Enabled = false;
                label7.Enabled = false;
                label8.Enabled = false;
                label9.Enabled = false;
                calendarFecha.Enabled = true;
                calendarFecha.Visible = true;
                calendarRango.Enabled = false;
                calendarRango.Visible = false;
                panelRangoInicio.Enabled = false;
                panelRangoFin.Enabled = false;
            }
        }

        private void cbRango_CheckedChanged(object sender, EventArgs e)
        {
            if (cbRango.Checked)
            {
                cbFecha.Checked = false;
                cbFecha.Enabled = true;
                cbRango.Enabled = false;
                cbFechaInicial.Enabled = true;
                cbFechaFinal.Enabled = true;
                cbFechaInicial.Visible = true;
                cbFechaFinal.Visible = true;
                txbDiaFecha.Enabled = false;
                cboxMesFecha.Enabled = false;
                txbYearFecha.Enabled = false;
                txbDiaRangoInicio.Enabled = true;
                cboxMesRangoInicio.Enabled = true;
                txbYearRangoInicio.Enabled = true;
                txbDiaRangoFin.Enabled = true;
                cboxMesRangoFin.Enabled = true;
                txbYearRangoFin.Enabled = true;
                label1.Enabled = false;
                label2.Enabled = false;
                label3.Enabled = false;
                label4.Enabled = true;
                label5.Enabled = true;
                label6.Enabled = true;
                label7.Enabled = true;
                label8.Enabled = true;
                label9.Enabled = true;
                calendarFecha.Enabled = false;
                calendarFecha.Visible = false;
                calendarRango.Enabled = true;
                calendarRango.Visible = true;
                panelRangoInicio.Enabled = true;
                panelRangoFin.Enabled = true;
            }
        }

        public string mes(string mes)
        {
            switch (mes)
            {
                case "1":
                    return "Enero";
                case "2":
                    return "Febrero";
                case "3":
                    return "Marzo";
                case "4":
                    return "Abril";
                case "5":
                    return "Mayo";
                case "6":
                    return "Junio";
                case "7":
                    return "Julio";
                case "8":
                    return "Agosto";
                case "9":
                    return "Septiembre";
                case "10":
                    return "Octubre";
                case "11":
                    return "Noviembre";
                case "12":
                    return "Diciembre";
                default:
                    return "";
            }
        }

        #region Fecha

        private void txbDiaFecha_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txbDiaFecha.Text.Length == 1)
                {
                    string first = txbDiaFecha.Text[0].ToString();
                    if (!Char.IsDigit(first[0]))
                    {
                        txbDiaFecha.Text = txbDiaFecha.Text.Substring(0, 0);
                    }
                    else if (Convert.ToInt32(first) > 3 && txbDiaFecha.Text.Length < 2)
                    {
                        txbDiaFecha.Text = txbDiaFecha.Text.Substring(0, 0);
                    }
                    else if (first == "3" && cboxMesFecha.Text == "Febrero")
                    {
                        txbDiaFecha.Text = txbDiaFecha.Text.Substring(0, 0);
                    }
                }
                else if (txbDiaFecha.Text.Length > 1)
                {
                    string first = txbDiaFecha.Text[0].ToString();
                    char letter = txbDiaFecha.Text[txbDiaFecha.Text.Length - 1];
                    if (!Char.IsDigit(letter) || (first == "3" && Convert.ToInt32(letter.ToString()) > 1) || (first == "0" && Convert.ToInt32(letter.ToString()) == 0))
                    {
                        txbDiaFecha.Text = txbDiaFecha.Text.Substring(0, txbDiaFecha.Text.Length - 1);
                    }
                    else if (first == "3" && letter == '1' && (cboxMesFecha.Text == "Abril") || cboxMesFecha.Text == "Junio" || cboxMesFecha.Text == "Septiembre" || cboxMesFecha.Text == "Noviembre")
                    {
                        txbDiaFecha.Text = txbDiaFecha.Text.Substring(0, txbDiaFecha.Text.Length - 1);
                    }
                    txbDiaFecha.SelectionStart = txbDiaFecha.Text.Length;
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(this, ex.Message, "Día inicial de rango");
            }
        }

        private void txbYearFecha_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txbYearFecha.Text.Length == 1)
                {
                    char letter = txbYearFecha.Text[0];
                    if (!Char.IsDigit(letter))
                    {
                        txbYearFecha.Text = txbYearFecha.Text.Substring(0, 0);
                    }
                }
                else if (txbYearFecha.Text.Length > 1)
                {
                    char letter = txbYearFecha.Text[txbYearFecha.Text.Length - 1];
                    if (!Char.IsDigit(letter))
                    {
                        txbYearFecha.Text = txbYearFecha.Text.Substring(0, txbYearFecha.Text.Length - 1);
                    }
                    txbYearFecha.SelectionStart = txbYearFecha.Text.Length;
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(this, ex.Message, "Año de fecha");
            }
        }

        private void calendarFecha_DateChanged(object sender, DateRangeEventArgs e)
        {
            txbDiaFecha.Text = calendarFecha.SelectionRange.Start.Day.ToString();
            cboxMesFecha.Text = mes(calendarFecha.SelectionRange.Start.Month.ToString());
            txbYearFecha.Text = calendarFecha.SelectionRange.Start.Year.ToString();
        }

        private void cboxMesFecha_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboxMesFecha.Text.Length == 1)
                {
                    char letter = cboxMesFecha.Text[0];
                    if (Char.IsDigit(letter))
                    {
                        cboxMesFecha.Text = cboxMesFecha.Text.Substring(0, 0);
                    }
                }
                else if (cboxMesFecha.Text.Length > 1)
                {
                    char letter = cboxMesFecha.Text[cboxMesFecha.Text.Length - 1];
                    if (Char.IsDigit(letter))
                    {
                        cboxMesFecha.Text = cboxMesFecha.Text.Substring(0, cboxMesFecha.Text.Length - 1);
                    }
                    cboxMesFecha.SelectionStart = cboxMesFecha.Text.Length;

                    //Checar mes y cambiar dia si se pasa del limite
                    if (cboxMesFecha.Text == "Febrero" && txbDiaRangoInicio.Text != "")
                    {
                        if (Convert.ToInt32(txbDiaFecha.Text) >= 30)
                        {
                            txbDiaFecha.Text = "28";
                        }
                    }
                    else if ((cboxMesFecha.Text == "Abril" || cboxMesFecha.Text == "Junio" || cboxMesFecha.Text == "Septiembre" || cboxMesFecha.Text == "Noviembre") && txbDiaFecha.Text != "")
                    {
                        if (Convert.ToInt32(txbDiaRangoInicio.Text) == 31)
                        {
                            txbDiaFecha.Text = "30";
                        }
                    }
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(this, ex.Message, "Mes Fecha");
            }
        }

        #endregion

        #region Rangos

        #region Rango Inicio

        private void txbDiaRangoInicio_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txbDiaRangoInicio.Text.Length == 1)
                {
                    string first = txbDiaRangoInicio.Text[0].ToString();
                    if (!Char.IsDigit(first[0]))
                    {
                        txbDiaRangoInicio.Text = txbDiaRangoInicio.Text.Substring(0, 0);
                    }
                    else if (Convert.ToInt32(first) > 3 && txbDiaRangoInicio.Text.Length < 2)
                    {
                        txbDiaRangoInicio.Text = txbDiaRangoInicio.Text.Substring(0, 0);
                    }
                    else if (first == "3" && cboxMesFecha.Text == "Febrero")
                    {
                        txbDiaRangoInicio.Text = txbDiaRangoInicio.Text.Substring(0, 0);
                    }
                }
                else if (txbDiaRangoInicio.Text.Length > 1)
                {
                    string first = txbDiaRangoInicio.Text[0].ToString();
                    char letter = txbDiaRangoInicio.Text[txbDiaRangoInicio.Text.Length - 1];
                    if (!Char.IsDigit(letter) || (first == "3" && Convert.ToInt32(letter.ToString()) > 1) || (first == "0" && Convert.ToInt32(letter.ToString()) == 0))
                    {
                        txbDiaRangoInicio.Text = txbDiaRangoInicio.Text.Substring(0, txbDiaRangoInicio.Text.Length - 1);
                    }
                    else if (first == "3" && letter == '1' && (cboxMesRangoInicio.Text == "Abril") || cboxMesRangoInicio.Text == "Junio" || cboxMesRangoInicio.Text == "Septiembre" || cboxMesRangoInicio.Text == "Noviembre")
                    {
                        txbDiaRangoInicio.Text = txbDiaRangoInicio.Text.Substring(0, txbDiaRangoInicio.Text.Length - 1);
                    }
                    txbDiaRangoInicio.SelectionStart = txbDiaRangoInicio.Text.Length;
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(this, ex.Message, "Día inicial de rango");
            }
        }

        private void txbYearRangoInicio_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txbYearRangoInicio.Text.Length == 1)
                {
                    char letter = txbYearRangoInicio.Text[0];
                    if (!Char.IsDigit(letter))
                    {
                        txbYearRangoInicio.Text = txbYearRangoInicio.Text.Substring(0, 0);
                    }
                }
                else if (txbYearRangoInicio.Text.Length > 1)
                {
                    char letter = txbYearRangoInicio.Text[txbYearRangoInicio.Text.Length - 1];
                    if (!Char.IsDigit(letter))
                    {
                        txbYearRangoInicio.Text = txbYearRangoInicio.Text.Substring(0, txbYearRangoInicio.Text.Length - 1);
                    }
                    txbYearRangoInicio.SelectionStart = txbYearRangoInicio.Text.Length;
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(this, ex.Message, "Año final de rango");
            }
        }

        private void cboxMesRangoInicio_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboxMesRangoInicio.Text.Length == 1)
                {
                    char letter = cboxMesRangoInicio.Text[0];
                    if (Char.IsDigit(letter))
                    {
                        cboxMesRangoInicio.Text = cboxMesRangoInicio.Text.Substring(0, 0);
                    }
                }
                else if (cboxMesRangoInicio.Text.Length > 1)
                {
                    char letter = cboxMesRangoInicio.Text[cboxMesRangoInicio.Text.Length - 1];
                    if (Char.IsDigit(letter))
                    {
                        cboxMesRangoInicio.Text = cboxMesRangoInicio.Text.Substring(0, cboxMesRangoInicio.Text.Length - 1);
                    }
                    cboxMesRangoInicio.SelectionStart = cboxMesRangoInicio.Text.Length;

                    //Checar mes y cambiar dia si se pasa del limite
                    if (cboxMesRangoInicio.Text == "Febrero" && txbDiaRangoInicio.Text != "")
                    {
                        if (Convert.ToInt32(txbDiaRangoInicio.Text) >= 30)
                        {
                            txbDiaRangoFin.Text = "28";
                        }
                    }
                    else if ((cboxMesRangoInicio.Text == "Abril" || cboxMesRangoInicio.Text == "Junio" || cboxMesRangoInicio.Text == "Septiembre" || cboxMesRangoInicio.Text == "Noviembre") && txbDiaRangoInicio.Text != "")
                    {
                        if (Convert.ToInt32(txbDiaRangoInicio.Text) == 31)
                        {
                            txbDiaRangoFin.Text = "30";
                        }
                    }
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(this, ex.Message, "Mes Rango Inicio");
            }
        }

        #endregion

        #region Rango Fin

        private void txbDiaRangoFin_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txbDiaRangoFin.Text.Length == 1)
                {
                    string first = txbDiaRangoFin.Text[0].ToString();
                    if (!Char.IsDigit(first[0]))
                    {
                        txbDiaRangoFin.Text = txbDiaRangoFin.Text.Substring(0, 0);
                    }
                    else if (Convert.ToInt32(first) > 3 && txbDiaRangoFin.Text.Length < 2)
                    {
                        txbDiaRangoFin.Text = txbDiaRangoFin.Text.Substring(0, 0);
                    }
                    else if (first == "3" && cboxMesFecha.Text == "Febrero")
                    {
                        txbDiaRangoFin.Text = txbDiaRangoFin.Text.Substring(0, 0);
                    }
                }
                else if (txbDiaRangoFin.Text.Length > 1)
                {
                    string first = txbDiaRangoFin.Text[0].ToString();
                    char letter = txbDiaRangoFin.Text[txbDiaRangoFin.Text.Length - 1];
                    if (!Char.IsDigit(letter) || (first == "3" && Convert.ToInt32(letter.ToString()) > 1) || (first == "0" && Convert.ToInt32(letter.ToString()) == 0))
                    {
                        txbDiaRangoFin.Text = txbDiaRangoFin.Text.Substring(0, txbDiaRangoFin.Text.Length - 1);
                    }
                    else if (first == "3" && letter == '1' && (cboxMesRangoFin.Text == "Abril") || cboxMesRangoFin.Text == "Junio" || cboxMesRangoFin.Text == "Septiembre" || cboxMesRangoFin.Text == "Noviembre")
                    {
                        txbDiaRangoFin.Text = txbDiaRangoFin.Text.Substring(0, txbDiaRangoFin.Text.Length - 1);
                    }
                    txbDiaRangoFin.SelectionStart = txbDiaRangoFin.Text.Length;
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(this, ex.Message, "Día inicial de rango");
            }
        }

        private void txbYearRangoFin_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txbYearRangoFin.Text.Length == 1)
                {
                    char letter = txbYearRangoFin.Text[0];
                    if (!Char.IsDigit(letter))
                    {
                        txbYearRangoFin.Text = txbYearRangoFin.Text.Substring(0, 0);
                    }
                }
                else if (txbYearRangoFin.Text.Length > 1)
                {
                    char letter = txbYearRangoFin.Text[txbYearRangoFin.Text.Length - 1];
                    if (!Char.IsDigit(letter))
                    {
                        txbYearRangoFin.Text = txbYearRangoFin.Text.Substring(0, txbYearRangoFin.Text.Length - 1);
                    }
                    txbYearRangoFin.SelectionStart = txbYearRangoFin.Text.Length;
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(this, ex.Message, "Año inicial de rango");
            }
        }

        private void cboxMesRangoFin_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboxMesRangoFin.Text.Length == 1)
                {
                    char letter = cboxMesRangoFin.Text[0];
                    if (Char.IsDigit(letter))
                    {
                        cboxMesRangoFin.Text = cboxMesRangoFin.Text.Substring(0, 0);
                    }
                }
                else if (cboxMesRangoFin.Text.Length > 1)
                {
                    char letter = cboxMesRangoFin.Text[cboxMesRangoFin.Text.Length - 1];
                    if (Char.IsDigit(letter))
                    {
                        cboxMesRangoFin.Text = cboxMesRangoFin.Text.Substring(0, cboxMesRangoFin.Text.Length - 1);
                    }
                    cboxMesRangoFin.SelectionStart = cboxMesRangoFin.Text.Length;

                    //Checar mes y cambiar dia si se pasa del limite
                    if (cboxMesRangoFin.Text == "Febrero" && txbDiaRangoFin.Text != "")
                    {
                        if (Convert.ToInt32(txbDiaRangoFin.Text) >= 30)
                        {
                            txbDiaRangoFin.Text = "28"; 
                        }
                    }
                    else if ((cboxMesRangoFin.Text == "Abril" || cboxMesRangoFin.Text == "Junio" || cboxMesRangoFin.Text == "Septiembre" || cboxMesRangoFin.Text == "Noviembre") && txbDiaRangoFin.Text != "")
                    {
                        if (Convert.ToInt32(txbDiaRangoFin.Text) == 31)
                        {
                            txbDiaRangoFin.Text = "30"; 
                        }
                    }
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show(this, ex.Message, "Mes Rango Fin");
            }
        }

        #endregion 

        private void calendarRango_DateChanged(object sender, DateRangeEventArgs e)
        {
            if (cbFechaInicial.Checked)
            {
                txbDiaRangoInicio.Text = calendarRango.SelectionRange.Start.Day.ToString();
                cboxMesRangoInicio.Text = mes(calendarRango.SelectionRange.Start.Month.ToString());
                txbYearRangoInicio.Text = calendarRango.SelectionRange.Start.Year.ToString();
                fechaInicial = calendarRango.SelectionRange.Start;
            }
            else if(cbFechaFinal.Checked)
            {
                txbDiaRangoFin.Text = DateTime.Now.Day.ToString();
                cboxMesRangoFin.Text = mes(calendarRango.SelectionRange.Start.Month.ToString());
                txbYearRangoFin.Text = calendarRango.SelectionRange.Start.Year.ToString();
                fechaFinal = calendarRango.SelectionRange.Start;
            }
        }

        #endregion

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (cbFecha.Checked)
            {
                if (txbDiaFecha.Text != "" && cboxMesFecha.Text != "" && txbYearFecha.Text != "")
                {
                    val = true;
                    fecha = txbDiaFecha.Text + "/" + cboxMesFecha.Text + "/" + txbYearFecha.Text;
                    btnOK.DialogResult = DialogResult.OK;
                    this.Hide();
                }
                else
                {
                    val = false;
                    MessageBox.Show(this, "Complete todos los campos", "Fecha");
                }
            }
            else 
            {
                if (txbDiaRangoInicio.Text != "" && cboxMesRangoInicio.Text != "" && txbYearRangoInicio.Text != "" && txbDiaRangoFin.Text != "" && cboxMesRangoFin.Text != "" && txbYearRangoFin.Text != "")
                {
                    if (fechaInicial < fechaFinal)
                    {
                        val = true;
                        fecha = txbDiaRangoInicio.Text + "/" + cboxMesRangoInicio.Text + "/" + txbYearRangoInicio.Text + " - " + txbDiaRangoFin.Text + "/" + cboxMesRangoFin.Text + "/" + txbYearRangoFin.Text;
                        btnOK.DialogResult = DialogResult.OK;
                        this.Hide(); 
                    }
                    else
                    {
                        MessageBox.Show(this, "La fecha final no puede ser anterior a la fecha inicial", "Fechas no permitidas");
                    }
                }
                else
                {
                    val = false;
                    MessageBox.Show(this, "Complete todos los campos", "Rango");
                }
            }
        }

        private void formCalendario_Load(object sender, EventArgs e)
        {
            txbDiaFecha.Text = calendarRango.SelectionRange.Start.Day.ToString();
            cboxMesFecha.Text = mes(calendarRango.SelectionRange.Start.Month.ToString());
            txbYearFecha.Text = calendarRango.SelectionRange.Start.Year.ToString();
            btnOK.DialogResult = DialogResult.None;
        }

        private void CbFechaInicial_Click(object sender, EventArgs e)
        {
            if (cbFechaFinal.Checked)
                cbFechaInicial.Checked = true;
            cbFechaFinal.Checked = false;
        }

        private void CbFechaFinal_Click(object sender, EventArgs e)
        {
            cbFechaInicial.Checked = false;
            if (cbFechaInicial.Checked)
                cbFechaFinal.Checked = true;
        }

        private void CboxMesRangoFin_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }

}
