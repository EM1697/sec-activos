﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;

namespace SEC_Activos
{
    public partial class formLogIn : Form
    {
        public formLogIn()
        {
            InitializeComponent();
        }

        #region Variables

        public MySqlConnection con = new MySqlConnection("SERVER=localhost;DATABASE=sec_activos;" + "UID=EMM;" + "PASSWORD=ACDC;");
        formExportar fExportar = new formExportar();
        public string user = "";
        public bool validUser = false;

        #endregion

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            if (Validar(txbUser.Text, MD5Hash(txbPsw.Text)))
            {
                this.Hide();
                new formExportar(user).Show(); 
            }
            else
            {
                MessageBox.Show(this, "Usuario o contraseña incorrectos", "Acceso incorrecto");
            }
        }

        public bool Validar(string user, string psw)
        {
            string usPasw = null;
            bool acceder = false;
            string query = "SELECT * FROM usuarios WHERE USER_USUARIO = '" + user + "'";
            try
            {
                con.Open();
                MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(query, con);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    usPasw = reader["USER_PWS"].ToString();
                    string nombre = reader["USER_NOMBRE"].ToString();
                    string aPat = reader["USER_APATERNO"].ToString();
                    string aMat = reader["USER_AMATERNO"].ToString();
                    this.user = nombre + ' ' + aPat + ' ' + aMat;
                }
                con.Close();
                if (user == "admin" && psw == usPasw)
                {
                    acceder = true;
                    validUser = true;
                }
                else
                {
                    acceder = false;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(this, e.Message, "Eror Log in");
                con.Close();
            }
            return acceder;
        }

        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input + "S0n0r4MX78e3e3"));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        private void FormLogIn_Shown(object sender, EventArgs e)
        {
            txbUser.Focus();
        }

        private void FormLogIn_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void FormLogIn_Load(object sender, EventArgs e)
        {

        }
    }
}
